# User Manual

## In a directory of your choice, execute the following commands

* git clone https://gitlab.com/formigoni-ufv/nml/qca-standard && cd qca-standard
* mkdir build && cd build
* cmake .. && make
* chmod +x main

## The software is now compiled, its usage is exemplified bellow

./main  -f inputQcaFile -o outputfile.txt
