#pragma once
#include <stdexcept>
#include <vector>
#include <fstream>
#include "genericqcacell.h"

class GenericQcaCellGrid {
	private:
		std::vector<std::vector<GenericQcaCell>> grid;
	public:
		void add(const GenericQcaCell& cell, const unsigned& x, const unsigned& y);
		void print(std::ostream& output) const;
};
